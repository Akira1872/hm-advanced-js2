
const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
try{
  //виводить відсутній елемент в консоль 
function findItem (item){
  const keys = ['author', 'name', 'price'];
     for (const key of keys) {
      if (!(key in item))
        throw new SyntaxError(`Сode cannot find the '${key}`);
  }
  }

}catch(err){
  console.error(err.massage);

}finally{
// виводить список на екран 
function createListBooks (){
  let ul = document.getElementById('root');
    books.forEach((e) => {
      const {author, name, price } = e;
      if(author && name && price){
      const li = document.createElement('li');
      li.textContent = (`author: ${author}, name: ${name}, price: ${price}`); 
      ul.append(li);
      }
    }
  )}
  createListBooks();  
}

findItem(books);
